# Idle Wizard #

Idle Wizard is a simple idle game about a single wizard, with his trusty Familliars, battling through monsters through an endless dungeon.

This project is planned to be done in one month, used as a training project for Koei Tecmo Software Vietnam - R&D Department in 2021.

The game is made with mobile controls in mind, but will be built for Windows due to constraints of time and demo purposes.

ClickUp Progress Link: https://app.clickup.com/7511641/v/g/4-19521840-7

### Basic Gameplay ###

The game is an idle game. Tap on the screen to shoot, or let the Famillars automatically shoot for you.

### Skills & Abilities ###

* Wizard Shot
* Familliars

### Gameplay Mechanics ###

* Wizard Shot
* Familliars
* Dungeons
* Bosses

### Monetizations ###

* Gacha

### Assets & Licenses ###

All of the assets used in this game (including music, UI, images....) belong to their respective owners, and will be fully credited below:

a. Music

- https://www.bensound.com/royalty-free-music/track/enigmatic

b. SFX

- https://soundbible.com/419-Tiny-Button-Push.html
- https://soundbible.com/472-Laser-Blasts.html

c. UI

- https://www.freepik.com/free-vector/medieval-game-interface_2874386.htm - Designed by macrovector
- https://www.flaticon.com/packs/simple-icon-set - Designed by Lyolya
- https://freepsdfiles.net/buttons/free-psd-mobile-game-gui

d. Images

- https://opengameart.org/content/top-down-wizard
- https://opengameart.org/content/bullet-collection-1-m484
- https://opengameart.org/content/bullet-collection-2-m484-games
- https://rvros.itch.io/pixel-art-animated-slime
- https://craftpix.net/freebies/free-rpg-monster-sprites-pixel-art/
- https://raventale.itch.io/daily-doodles-pixelart-asset-pack
- https://limezu.itch.io/21rpg-battlers
- https://pipoya.itch.io/free-rpg-monster-pack
- https://limezu.itch.io/rpg-arsenal
- https://graphicburger.com/mobile-game-gui/