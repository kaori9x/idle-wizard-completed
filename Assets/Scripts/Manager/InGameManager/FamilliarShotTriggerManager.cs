using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FamilliarShotTriggerManager : MonoBehaviour
{
    /** ======= MARK: - Init ======= */
    public string GetClassName()
    {
        return this.GetType().Name;
    }

    private FamilliarShotTriggerManager()
    {
        if (_instance == null)
            _instance = this;
    }

    /** ======= MARK: - Singleton ======= */

    private static FamilliarShotTriggerManager _instance;

    public static FamilliarShotTriggerManager instance
    {
        get
        {
            return _instance;
        }
    }

    /** ======= MARK: - Fields and Properties ======= */

    //private List<Vector3> spawnPositionList = new List<Vector3>
    //{
    //    new Vector3(150.0f, 450.0f, 0.0f),
    //    new Vector3(270.0f, 300.0f, 0.0f),
    //    new Vector3(470.2f, 300.0f, 0.0f),
    //    new Vector3(600.0f, 450.0f, 0.0f)
    //};

    private List<Vector3> spawnPositionList = new List<Vector3>
    {
        new Vector3(700.0f, 350.0f, 0.0f),
        new Vector3(870.0f, 200.0f, 0.0f),
        new Vector3(1070.2f, 200.0f, 0.0f),
        new Vector3(1250.0f, 350.0f, 0.0f)
    };

    private bool familliar1Flag = false;
    private bool familliar2Flag = false;
    private bool familliar3Flag = false;
    private bool familliar4Flag = false;

    private List<FamilliarData> listCurrentEquippedFamilliars;

    [SerializeField]
    private GameObject familliarShotContainerParent;

    [SerializeField]
    private GameObject familliarShotContactObjectParent;

    [SerializeField]
    private GameObject familliarImageParentObject;

    /** ======= MARK: - MonoBehaviour Methods ======= */

    private void Awake()
    {
        AddListeners();
    }

    private void Start()
    {
        // HieuBT - Tat Init + ContactEffect cua Familliar Shot
        for (int i = 0; i < familliarShotContainerParent.transform.childCount; i++)
        {
            for (int j = 0; j < familliarShotContainerParent.transform.GetChild(i).childCount; j++)
            {
                Transform bullet = familliarShotContainerParent.transform.GetChild(i).GetChild(j);

                //if (bullet.GetComponent<BaseFamilliarShotController>() != null)
                //{
                //    bullet.GetComponent<BaseFamilliarShotController>().familliarIndex = i + 1;
                //}

                bullet.gameObject.SetActive(false);
            }
        }

        for (int i = 0; i < familliarShotContactObjectParent.transform.childCount; i++)
        {
            familliarShotContactObjectParent.transform.GetChild(i).gameObject.SetActive(false);
        }

        ActivateAllEquippedFamilliars();
    }

    private void Update()
    {
        // HieuBT - Turn off init animation of Familliar Shots
        for (int i = 0; i < familliarShotContainerParent.transform.childCount; i++)
        {
            Transform familliarShotInitObject = familliarShotContainerParent.transform.GetChild(i).GetChild(0);
            Animator animator = familliarShotInitObject.GetComponent<Animator>();
            if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !animator.IsInTransition(0))
            {
                familliarShotInitObject.gameObject.SetActive(false);
            }
        }

        // HieuBT - Turn off contact animation of Familliar Shots
        for (int i = 0; i < familliarShotContactObjectParent.transform.childCount; i++)
        {
            Animator animator2 = familliarShotContactObjectParent.transform.GetChild(i).GetComponent<Animator>();
            if (animator2.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !animator2.IsInTransition(0))
            {
                familliarShotContactObjectParent.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    private void OnDestroy()
    {
        //RemoveListeners();
    }

    /** ======= MARK: - Event Listeners ======= */

    private List<EventListener> _eventListeners;

    private void AddListeners()
    {
        _eventListeners = new List<EventListener>
        {
           CustomEventSystem.instance.AddListener(EventCode.ON_EQUIP_FAMILLIAR, this, OnEquipFamilliar),
           CustomEventSystem.instance.AddListener(EventCode.ON_UNEQUIP_FAMILLIAR, this, OnUnEquipFamilliar),
        };
    }

    private void RemoveListeners()
    {
        if (_eventListeners.Count != 0)
        {
            foreach (EventListener listener in _eventListeners)
                CustomEventSystem.instance.RemoveListener(listener.eventCode, listener);
        }
    }

    /** ======= MARK: - Listeners ======= */

    private void OnEquipFamilliar(object[] eventParam)
    {
        ActivateAllEquippedFamilliars();
    }
    private void OnUnEquipFamilliar(object[] eventParam)
    {
        ActivateAllEquippedFamilliars();
    }

    /** ======= MARK: - Trigger Auto Shot Coroutines ======= */

    private void ActivateAllEquippedFamilliars()
    {
        // HieuBT: - Get Familliar List 
        List<FamilliarData> inventoryList = SaveDataManager.instance.LoadFamilliarInventoryData();
        if (inventoryList.Count == 0) return;
        List<int> equippedIndex = SaveDataManager.instance.LoadEquippedFamilliarData();
        if (equippedIndex.Count == 0)
        {
            for(int i = 0; i < 4; i++)
            {
                equippedIndex.Add(-1);
            }
        }


        listCurrentEquippedFamilliars = new List<FamilliarData>();

        for (int i = 0; i < equippedIndex.Count; i++)
        {
            listCurrentEquippedFamilliars.Add(null);
            if (equippedIndex[i] >= 0)
            {
                listCurrentEquippedFamilliars[i] = inventoryList[i];
            }
        }

        // HieuBT: - Trigger luon Auto Shot voi list Familliar Hien tai

        if (equippedIndex[0] > 0) familliar1Flag = true; else familliar1Flag = false;
        if (equippedIndex[1] > 0) familliar2Flag = true; else familliar2Flag = false;
        if (equippedIndex[2] > 0) familliar3Flag = true; else familliar3Flag = false;
        if (equippedIndex[3] > 0) familliar4Flag = true; else familliar4Flag = false;

        TriggerAutoShot(equippedIndex);
    }
    public void TriggerAutoShot(List<int> indexList)
    {
        // HieuBT: - Familliar 1 Activation
        if (indexList[0] >= 0)
        {
            familliar1Flag = true;
            StartCoroutine(Familliar1Shot(indexList[0]));
        }
        else 
        {
            familliar1Flag = false;
            StopCoroutine(Familliar1Shot(indexList[0])); 
        }

        // HieuBT: - Familliar 2 Activation
        if (indexList[1] >= 0)
        {
            familliar2Flag = true;
            StartCoroutine(Familliar2Shot(indexList[1]));
        }
        else 
        {
            familliar2Flag = false;
            StopCoroutine(Familliar2Shot(indexList[1])); 
        }

        // HieuBT: - Familliar 3 Activation
        if (indexList[2] >= 0)
        {
            familliar3Flag = true;
            StartCoroutine(Familliar3Shot(indexList[2]));
        }
        else 
        {
            familliar3Flag = false;
            StopCoroutine(Familliar3Shot(indexList[2])); 
        }

        // HieuBT: - Familliar 4 Activation
        if (indexList[3] >= 0)
        {
            familliar4Flag = true;
            StartCoroutine(Familliar4Shot(indexList[3]));
        }
        else 
        {
            familliar4Flag = false;
            StopCoroutine(Familliar4Shot(indexList[3])); 
        }

        SetupFamilliarImage();
    }

    IEnumerator Familliar1Shot(int familliarIndex)
    {
        while (familliarIndex >= 0 && familliar1Flag == true)
        {
            DoShootFamilliarShots(0, spawnPositionList[0]);
            yield return new WaitForSeconds(1f);
        }
    }

    IEnumerator Familliar2Shot(int familliarIndex)
    {
        while (familliarIndex >= 0 && familliar2Flag == true)
        {
            DoShootFamilliarShots(1, spawnPositionList[1]);
            yield return new WaitForSeconds(1.5f);
        }
    }

    IEnumerator Familliar3Shot(int familliarIndex)
    {
        while (familliarIndex >= 0 && familliar3Flag == true)
        {
            DoShootFamilliarShots(2, spawnPositionList[2]);
            yield return new WaitForSeconds(1.75f);
        }
    }

    IEnumerator Familliar4Shot(int familliarIndex)
    {
        while (familliarIndex >= 0 && familliar4Flag == true)
        {
            DoShootFamilliarShots(3, spawnPositionList[3]);
            yield return new WaitForSeconds(2f);
        }
    }

    private void DoShootFamilliarShots(int equipIndex, Vector3 spawnPosition)
    {
        int lastActiveShotIndex = -1;
        Transform chosenFamilliarShotContainer = familliarShotContainerParent.transform.GetChild(equipIndex);
        for (int i = 1; i < chosenFamilliarShotContainer.childCount; i++)
        {
            if (chosenFamilliarShotContainer.GetChild(i).gameObject.activeSelf == false)
            {
                if (i == 0 || i == 1)
                {
                    lastActiveShotIndex = -1;
                }
                else
                {
                    lastActiveShotIndex = i - 1;
                }
                break;
            }
        }

        if ((lastActiveShotIndex == -1 && chosenFamilliarShotContainer.GetChild(1).gameObject.activeSelf == false)
            || lastActiveShotIndex != -1)
        {
            GameObject familliarShotInitObject = chosenFamilliarShotContainer.GetChild(0).gameObject;
            familliarShotInitObject.SetActive(true);
            familliarShotInitObject.transform.position = spawnPosition;

            SoundManager.instance.wizardShotSFXSource.PlayOneShot(SoundManager.instance.soundList.basicWizardShotSFX);
            int playingIndex = 0;
            if (lastActiveShotIndex == -1 || lastActiveShotIndex == 0)
            {
                playingIndex = 1;
            }
            else
            {
                playingIndex = lastActiveShotIndex + 1;
            }

            GameObject bullet = chosenFamilliarShotContainer.GetChild(playingIndex).gameObject;
            bullet.SetActive(true);
            bullet.transform.position = spawnPosition;
            bullet.GetComponent<BaseFamilliarShotController>().familliarData = listCurrentEquippedFamilliars[equipIndex];
            bullet.GetComponent<BaseFamilliarShotController>().SetupFamilliarShotStats();
            bullet.GetComponent<BaseFamilliarShotController>().SetupFamilliarShotColor();
            bullet.GetComponent<BaseFamilliarShotController>().SetupSpecialFamilliarShots();
        }
    }

    /** ======= MARK: - Familliar Images ======= */
    private void SetupFamilliarImage()
    {
        List<FamilliarData> inventoryList = SaveDataManager.instance.LoadFamilliarInventoryData();
        List<int> equipedList = SaveDataManager.instance.LoadEquippedFamilliarData();

        // Setup Familliar Image
        for (int i = 0; i < 4; i++)
        {
            familliarImageParentObject.transform.GetChild(i).gameObject.SetActive(true);
            Image familliarImage = familliarImageParentObject.transform.GetChild(i).GetComponent<Image>();
            familliarImage.sprite = Resources.Load<Sprite>(inventoryList[i].image);
            if (equipedList[i] < 0)
            {
                familliarImageParentObject.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    public void TestPrintFlag()
    {
        Debug.Log("xxxx Familliar 1 Flag: " + familliar1Flag + " Current index: " + listCurrentEquippedFamilliars[0].index);
        Debug.Log("xxxx Familliar 2 Flag: " + familliar2Flag + " Current index: " + listCurrentEquippedFamilliars[1].index);
        Debug.Log("xxxx Familliar 3 Flag: " + familliar3Flag + " Current index: " + listCurrentEquippedFamilliars[2].index);
        Debug.Log("xxxx Familliar 4 Flag: " + familliar4Flag + " Current index: " + listCurrentEquippedFamilliars[3].index);
    }
}
