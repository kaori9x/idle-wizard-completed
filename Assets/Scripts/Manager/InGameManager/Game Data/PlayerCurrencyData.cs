using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CurrencyDataRoot
{
    public CurrencyData currencyData;
}

[System.Serializable]
public class CurrencyData
{
    public string gold="0";
    public string ruby="0";
    public string diamond="0";
}


