using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GachaController : MonoBehaviour
{
    /** ======= MARK: - Fields & Properties ======= */

    private List<FamilliarData> currentFamilliarBannerPool;
    private List<FamilliarData> matchedRarityPool;

    public FamilliarData singleRoll;
    public List<FamilliarData> tenRolls;

    public GameObject popupSingleRoll;
    public GameObject popupTenRoll;
    public GameObject popupNotEnough;

    /** ======= MARK: - MonoBehaviour Methods ======= */

    private void Start()
    {
        currentFamilliarBannerPool = new List<FamilliarData>();
        GetBannerData("permanent_banner");
    }

    /** ======= MARK: - Banner Functions ======= */

    public void GetBannerData(string banner)
    {
        List<GachaBannerData> bannerList = JSONUtil.LoadDataFromJson<GachaBannerDataRoot>("GachaBanners").listGachaBanners;
        List<int> unitList = new List<int>();
        for (int i = 0; i < bannerList.Count; i++)
        {

            if (bannerList[i].name == banner)
            {
                unitList = bannerList[i].units;
                break;
            }
        }
        if (unitList.Count == 0)
        {
            Debug.Log("ERROR: Cannot find banner");
            return;
        }

        List<FamilliarData> listFamilliarData = GameFlowManager.instance.listAllFamilliarData;

        for (int i = 0; i < listFamilliarData.Count; i++)
        {
            for (int k = 0; k < unitList.Count; k++)
            {
                if (listFamilliarData[i].index == unitList[k])
                {
                    currentFamilliarBannerPool.Add(listFamilliarData[i]);
                }
            }
        }
    }   

    /** ======= MARK: - Gacha Functions ======= */
    public FamilliarData RollSingle()
    {
        if (!(LargeNumberStaticUtil.CompareNumber(CurrencyController.currency.diamond, "50")==LargeNumberResult.TRUE))
        {
            popupNotEnough.SetActive(true);
            popupNotEnough.transform.GetChild(1).GetComponent<Text>().text = "Not enough diamond!";
            return singleRoll;
        }
        singleRoll = null;
        string rarity = RollRandomRarity();
        if (CheckMatchedRarityPool(rarity))
        {
            // HieuBT: - Get Random item from matched rarity pool
            int randomIndex = Random.Range(0, matchedRarityPool.Count);
            singleRoll = matchedRarityPool[randomIndex];
        }

        // HieuBT - Bind rolled familliar to UI
        SetupGachaResultData(popupSingleRoll, 0, rarity, singleRoll);

        popupSingleRoll.SetActive(true);
        return singleRoll;
    }

    public List<FamilliarData> RollMultiple(int amount)
    {
        tenRolls.Clear();
        if (!(LargeNumberStaticUtil.CompareNumber(CurrencyController.currency.diamond, LargeNumberStaticUtil.Multiplication("50",amount))== LargeNumberResult.TRUE))
        {
            popupNotEnough.SetActive(true);
            popupNotEnough.transform.GetChild(1).GetComponent<Text>().text = "Not enough diamond!";
            return tenRolls;
        }
        for (int i = 0; i < amount; i++)
        {
            string rarity = RollRandomRarity();
            if (CheckMatchedRarityPool(rarity))
            {
                // HieuBT: - Get Random item from matched rarity pool
                int randomIndex = Random.Range(0, matchedRarityPool.Count );
                tenRolls.Add(matchedRarityPool[randomIndex]);
                if (i < 10) {
                    // HieuBT - Bind rolled familliar to UI
                    SetupGachaResultData(popupTenRoll, i, rarity, matchedRarityPool[randomIndex]);
                }
            }
        }
        popupTenRoll.SetActive(true);
        return tenRolls;
    }

    /** ======= MARK: - Handle Rarity ======= */

    public string RollRandomRarity()
    {
        int rarityPercent = Random.Range(0, 100);
        string rolledRarity = GetClosestRarity(rarityPercent);

        return rolledRarity;
    }

    private string GetClosestRarity(int percentage)
    {
        // Percentage la cai roll ra duoc
        Debug.Log("==== Percentage rolled: " + percentage);
        string[,] rarityList = new string[,] { 
            { "R", "80" }, 
            { "SR", "10" },
            { "SSR", "10" }
        };

        int currentPercentage = 0;

        for (int i = 0; i < rarityList.Length; i++)
        {
            currentPercentage += int.Parse(rarityList[i, 1]);
            if (currentPercentage >= percentage)
            {
                return rarityList[i, 0];
            }
        }

        return "R";
    }
    public bool CheckMatchedRarityPool(string rarity)
    {
        matchedRarityPool = new List<FamilliarData>();
        for (int j = 0; j < currentFamilliarBannerPool.Count; j++)
        {
            if (currentFamilliarBannerPool[j].rarity == rarity)
            {
                matchedRarityPool.Add(currentFamilliarBannerPool[j]);

            }
        }

        if (matchedRarityPool.Count <= 0)
        {
            Debug.Log("No matched items from gacha pool");
            return false;
        }

        return true;
    }

    /** ======= MARK: - Handle Rarity ======= */

    public void SetupGachaResultData(GameObject popUpObject, int currentFamilliarIndex, string rarity, FamilliarData data)
    {
        Transform rolledFamilliar = popUpObject.transform.GetChild(currentFamilliarIndex);

        Transform rarityImage = rolledFamilliar.transform.GetChild(0);
        Transform image = rolledFamilliar.transform.GetChild(1);
        Transform idText = rolledFamilliar.transform.GetChild(2);
        Transform rarityText = rolledFamilliar.transform.GetChild(3);

        image.GetComponent<Image>().sprite = Resources.Load<Sprite>(data.image);
        idText.GetComponent<Text>().text = data.name;
        rarityText.GetComponent<Text>().text = data.rarity;

        string rarityImageString = "";
        switch (rarity)
        {
            case "R":
                rarityImageString = "Sprites/UI/Frames/Silver_square";
                break;
            case "SR":
                rarityImageString = "Sprites/UI/Frames/Gold_square";
                break;
            case "SSR":
                rarityImageString = "Sprites/UI/Frames/Rainbow Square";
                break;
        }
        rarityImage.GetComponent<Image>().sprite = Resources.Load<Sprite>(rarityImageString);
    }
}