using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameFlowManager : MonoBehaviour
{
    /** ======= MARK: - Init ======= */
    public string GetClassName()
    {
        return this.GetType().Name;
    }

    private GameFlowManager()
    {
        if (_instance == null)
            _instance = this;
    }

    /** ======= MARK: - Singleton ======= */

    private static GameFlowManager _instance;

    public static GameFlowManager instance
    {
        get
        {
            return _instance;
        }
    }

    /** ======= MARK: - Fields and Properties ======= */

    private List<EventListener> _eventListeners;

    public GameObject targetShotPoint;

    [SerializeField]
    private GameObject shotContactObject;

    [SerializeField]
    private GachaController gachaController;

    public List<FamilliarData> listAllFamilliarData;
    public List<FamilliarData> listFamilliarInventoryData;
    public List<FamilliarData> listFamilliarEquippedData;

    public GameObject bulletContainer;

    /** ======= MARK: - MonoBehaviour Methods ======= */

    private void Awake()
    {
        AddListeners();

        LoadJSONData();
    }

    private void Start()
    {
        // HieuBT - Bat theme cua stage
        SoundManager.instance.backgroundMusicSource.clip = SoundManager.instance.soundList.stageTheme;
        SoundManager.instance.backgroundMusicSource.Play();

        // HieuBT - Tat Init + ContactEffect cua Wizard Shot
        for (int i = 0; i < bulletContainer.transform.childCount; i++)
        {
            Transform bullet = bulletContainer.transform.GetChild(i);
            bullet.gameObject.SetActive(false);
        }
        shotContactObject.SetActive(false);
    }

    private void Update()
    {
        // HieuBT - Turn off contact animation
        Animator animator1 = shotContactObject.GetComponent<Animator>();
        if (animator1.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !animator1.IsInTransition(0))
        {
            shotContactObject.SetActive(false);
        }

        // HieuBT: - Toggle Full Screen
        if (Input.GetKeyDown(KeyCode.F11))
        {
            Screen.fullScreen = !Screen.fullScreen;
        }
    }

    private void OnDestroy()
    {
        RemoveListeners();
    }

    /** ======= MARK: - Handle Events ======= */

    private void AddListeners()
    {
        _eventListeners = new List<EventListener>
        {

        };
    }

    private void RemoveListeners()
    {
        if (_eventListeners.Count != 0)
        {
            foreach (EventListener listener in _eventListeners)
                CustomEventSystem.instance.RemoveListener(listener.eventCode, listener);
        }
    }

    /** ======= MARK: - Load JSON Data ======= */

    private void LoadJSONData()
    {
        listAllFamilliarData = JSONUtil.LoadDataFromJson<FamilliarDataRoot>("FamilliarData").listFamilliarData;
    }

    // HieuBT - Test Gacha Functions

    public void Roll10UnitsFromPermanentBanner()
    {
        List<FamilliarData> listRolledUnits = gachaController.RollMultiple(10);
        if (!(LargeNumberStaticUtil.CompareNumber(CurrencyController.currency.diamond, "500")==LargeNumberResult.TRUE))
        {
            return;
        }
        CurrencyController.RemoveDiamond("500");
        SaveDataManager.instance.AddFamilliarFromGacha(listRolledUnits);
    }
    public void Roll1UnitsFromPermanentBanner(){
        FamilliarData RolledUnits = gachaController.RollSingle();
        if (!(LargeNumberStaticUtil.CompareNumber(CurrencyController.currency.diamond, "50") == LargeNumberResult.TRUE))
        {
            return;
        }
        List<FamilliarData> listRolledUnits = new List<FamilliarData>();
        listRolledUnits.Add(RolledUnits);
        CurrencyController.RemoveDiamond("50");
        SaveDataManager.instance.AddFamilliarFromGacha(listRolledUnits);
    }
}
