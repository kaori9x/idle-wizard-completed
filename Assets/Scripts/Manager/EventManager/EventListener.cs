using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventListener
{
    public string listenerId;
    public string eventCode;
    public object caller;
    public bool isEnabled;
    public bool isRemoved;
    public delegate void EventCallback(params object[] args);
    public Action<object[]> callback;

    public EventListener(string eventCode, string listenerId, object caller, Action<object[]> callback)
    {
        this.listenerId = listenerId;
        this.eventCode = eventCode;
        this.caller = caller;
        this.callback = callback;
        this.isEnabled = true;
        this.isRemoved = false;
    }
}