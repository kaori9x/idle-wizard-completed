using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WizardShotUpgradeController : MonoBehaviour
{
    /** ======= MARK: - Fields and Properties ======= */

    private static int BASE_WIZARD_SHOT_LEVEL = 1;

    private static string BASE_WIZARD_SHOT_POWER = "1.00";
    private static float POWER_MODIFIER = 2.0f;

    private static string BASE_WIZARD_SHOT_UPGRADE_COST = "5.00"; // Cost de upgrade tu Lvl 1 len Lvl 2 
    private static float COST_MODIFIER = 3.0f;

    public int currentWizardShotLevel;
    public string currentWizardShotPower;

    [SerializeField]
    private GameObject popupNotEnough;

    [SerializeField]
    private TextMeshProUGUI dpsLabel;

    [SerializeField]
    private TextMeshProUGUI currentLevelLabel;

    [SerializeField]
    private TextMeshProUGUI upgrade1CostLabel;

    [SerializeField]
    private TextMeshProUGUI upgrade10CostLabel;

    /** ======= MARK: - MonoBehaviour Methods ======= */

    // Start is called before the first frame update
    void Start()
    {
        //WizardShotData data = SaveDataManager.instance.LoadWizardShotData();
        //if (data != null)
        //{
        //    currentWizardShotLevel = data.currentLevel;
        //    currentWizardShotPower = data.currentPower;
        //} else
        //{
        //    currentWizardShotLevel = BASE_WIZARD_SHOT_LEVEL;
        //    currentWizardShotPower = BASE_WIZARD_SHOT_POWER;
        //}

        currentWizardShotLevel = BASE_WIZARD_SHOT_LEVEL;
        currentWizardShotPower = BASE_WIZARD_SHOT_POWER;
        currentLevelLabel.text = "Current Level: " + currentWizardShotLevel;
        dpsLabel.text = "Current DPS: " + currentWizardShotPower;
        upgrade1CostLabel.text = CalculateUpgradeCost(currentWizardShotLevel + 1);
        upgrade10CostLabel.text = CalculateTenMoreConsecutiveCost(currentWizardShotLevel + 1);
    }

    /** ======= MARK: - Upgrade Methods ======= */

    public void UpgradeWizardShot()
    {
        DoUpgradeWizardShotByLevel(1);
    }
    public void UpgradeWizardShot10()
    {
        DoUpgradeWizardShotByLevel(10);
    }

    public void DoUpgradeWizardShotByLevel(int level)
    {
        currentWizardShotLevel += level;
        string upgradeCost = CalculateUpgradeCost(currentWizardShotLevel); // Level sau khi da cong

        if (LargeNumberStaticUtil.CompareNumber(CurrencyController.currency.gold, upgradeCost)==LargeNumberResult.FALSE)
        {
            popupNotEnough.SetActive(true);
            popupNotEnough.transform.GetChild(1).GetComponent<Text>().text = "Not enough gold!";
            return;
        }

        currentWizardShotPower = CalculateWizardShotPower(currentWizardShotLevel); // Level sau khi da cong

        WizardShotData data = new WizardShotData(currentWizardShotLevel, currentWizardShotPower);
        SaveDataManager.instance.SaveWizardShotData(data);

        currentLevelLabel.text = "Current Level: " + currentWizardShotLevel;
        dpsLabel.text = "Current DPS: " + currentWizardShotPower;
        upgrade1CostLabel.text = CalculateUpgradeCost(currentWizardShotLevel + 1);
        upgrade10CostLabel.text = CalculateTenMoreConsecutiveCost(currentWizardShotLevel + 1);

        CurrencyController.RemoveGold(upgradeCost);
    }

    /** ======= MARK: - Calculation Methods ======= */

    private string CalculateWizardShotPower(int wizardShotLevel)
    {
        string result = "0";
        float powerCurrent = Mathf.Pow(POWER_MODIFIER, wizardShotLevel / 10);

        string convertedLevel = LargeNumberStaticUtil.ConvertNumber(wizardShotLevel.ToString());
        result = LargeNumberStaticUtil.Multiplication(convertedLevel, powerCurrent);

        return result;
    }

    private string CalculateUpgradeCost(int wizardShotLevel) // Tinh toan cost de upgrade len level la wizardShotLevel
    {
        string result = "0";
        string additive = LargeNumberStaticUtil.Subtraction(BASE_WIZARD_SHOT_UPGRADE_COST, "1.00");
        float powerCurrent = Mathf.Pow(COST_MODIFIER, wizardShotLevel / 10);

        string convertedLevel = LargeNumberStaticUtil.ConvertToRoundNumber(wizardShotLevel.ToString());
        result = LargeNumberStaticUtil.Subtraction(convertedLevel, "1.00");
        result = LargeNumberStaticUtil.Addition(result, additive);
        result = LargeNumberStaticUtil.Multiplication(result, powerCurrent);

        return result;
    }

    private string CalculateTenMoreConsecutiveCost(int wizardShotLevel) // Tinh toan cost de upgrade len 10 level, tinh tu level be hon input
                                                                           // Vi du: CalculateTenMoreConsecutiveCost(2) la tinh tong upgrade cost cho den level 12
    {
        string result = "0";

        for (int i = 0; i < 10; i++)
        {
            result = LargeNumberStaticUtil.Addition(result, CalculateUpgradeCost(wizardShotLevel + i));
        }

        return result;
    }
}
