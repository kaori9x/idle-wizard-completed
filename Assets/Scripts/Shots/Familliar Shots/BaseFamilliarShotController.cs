using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseFamilliarShotController : MonoBehaviour
{
    /** ======= MARK: - Fields and Properties ======= */

    private List<EventListener> _eventListeners;

    private Vector3 endMovePoint;

    [SerializeField]
    private FamilliarShotAllImage listAllFamilliarShotImage;

    [SerializeField]
    private FamilliarShotContactImageAllObject listAllFamilliarShotContactObject;

    [SerializeField]
    private FamilliarShotStats familliarShotStats;

    public FamilliarData familliarData;

    /** ======= MARK: - MonoBehavior Functions ======= */

    void Start()
    {
        //AddListeners();

        endMovePoint = transform.TransformPoint(0.0f, 500.0f, 0.0f);
    }

    void Update()
    {
        Vector3 targetedPosition = GameFlowManager.instance.targetShotPoint.transform.position;
        float flyStep = familliarShotStats.moveSpeed * Time.deltaTime;

        if (Vector3.Distance(transform.position, targetedPosition) > flyStep)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetedPosition, flyStep);
        }
        else
        {
            if (GUIManager.instance.gameObject.GetComponent<BackgroundNextStageController>().isCameraMoveRunning)
            {
                transform.gameObject.SetActive(false);
                return;
            }

            SoundManager.instance.wizardShotContactSFXSource.PlayOneShot(SoundManager.instance.soundList.wizardShotContactSFX);
            transform.gameObject.SetActive(false);

            SetActiveCorrespondingShotContactObject();

            CustomEventSystem.instance.DispatchEvent(EventCode.ON_SHOT_CONTACT, new object[] {
                ShotType.FAMILLIAR,
                familliarShotStats.shotDamage
            });
        }
    }

    /** ======= MARK: - Event Listeners ======= */

    private void AddListeners()
    {
        _eventListeners = new List<EventListener>
        {

        };
    }

    private void RemoveListeners()
    {
        if (_eventListeners.Count != 0)
        {
            foreach (EventListener listener in _eventListeners)
                CustomEventSystem.instance.RemoveListener(listener.eventCode, listener);
        }
    }

    /** ======= MARK: - Setup ======= */

    public void SetupFamilliarShotColor()
    {
        if (familliarData.type == "Spread")
        {
            GetComponent<RectTransform>().localScale = new Vector3(1.5f, 1.5f, 1.0f);
            SetupFamilliarShotSpread2Color();
        }
        else if (familliarData.type == "Rapid")
        {
            GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
            SetupFamilliarShotNormalColor();
        }
        else if (familliarData.type == "Burst")
        {
            GetComponent<RectTransform>().localScale = new Vector3(3.0f, 3.0f, 1.0f);
            SetupFamilliarShotNormalColor();
        }
    }

    public void SetupFamilliarShotStats()
    {
        familliarShotStats = new FamilliarShotStats();

        familliarShotStats.shotDamage = familliarData.stats.baseDamage;
        //familliarShotStats.CRTDamage = familliarData.stats.baseDamage;

        familliarShotStats.CRTRate = float.Parse(familliarData.stats.CRTRate);
        familliarShotStats.moveSpeed = float.Parse(familliarData.stats.shotSpeed);
        familliarShotStats.shotSize = float.Parse(familliarData.stats.shotSize);
        familliarShotStats.startDelay = float.Parse(familliarData.stats.shotDelay);
    }

    private void SetActiveCorrespondingShotContactObject()
    {
        switch (familliarData.stats.shotColor)
        {
            case "green":
                listAllFamilliarShotContactObject.familliarShotContactGreenObject.SetActive(true);
                break;
            case "blue":
                listAllFamilliarShotContactObject.familliarShotContactBlueObject.SetActive(true);
                break;
            case "orange":
                listAllFamilliarShotContactObject.familliarShotContactOrangeObject.SetActive(true);
                break;
            case "violet":
                listAllFamilliarShotContactObject.familliarShotContactVioletObject.SetActive(true);
                break;
            case "red":
                listAllFamilliarShotContactObject.familliarShotContactRedObject.SetActive(true);
                break;
            case "white":
                listAllFamilliarShotContactObject.familliarShotContactWhiteObject.SetActive(true);
                break;
            case "pink":
                listAllFamilliarShotContactObject.familliarShotContactPinkObject.SetActive(true);
                break;
            case "yellow":
                listAllFamilliarShotContactObject.familliarShotContactYellowObject.SetActive(true);
                break;
        }
    }

    /** ======= MARK: - Setup Special Familliar Shots ======= */
    public void SetupSpecialFamilliarShots()
    {
        if (familliarData.type == "Spread")
        {
            GetComponent<RectTransform>().localScale = new Vector3(2.0f, 2.0f, 1.0f);
        } else if (familliarData.type == "Rapid")
        {
            GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
        } else if (familliarData.type == "Burst")
        {
            GetComponent<RectTransform>().localScale = new Vector3(3.0f, 3.0f, 1.0f);
        }
    }

    /** ======= MARK: - Setup Familliar Shot Color ======= */
    public void SetupFamilliarShotNormalColor()
    {
        switch (familliarData.stats.shotColor)
        {
            case "green":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotGreenNormal;
                break;
            case "blue":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotBlueNormal;
                break;
            case "orange":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotOrangeNormal;
                break;
            case "purple":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotVioletNormal;
                break;
            case "red":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotRedNormal;
                break;
            case "white":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotWhiteNormal;
                break;
            case "pink":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotPinkNormal;
                break;
            case "yellow":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotBlueNormal;
                break;
        }
    }

    public void SetupFamilliarShotSpread2Color()
    {
        switch (familliarData.stats.shotColor)
        {
            case "green":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotGreenSpread2;
                break;
            case "blue":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotBlueSpread2;
                break;
            case "orange":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotOrangeSpread2;
                break;
            case "purple":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotVioletSpread2;
                break;
            case "red":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotRedSpread2;
                break;
            case "white":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotWhiteSpread2;
                break;
            case "pink":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotPinkSpread2;
                break;
            case "yellow":
                GetComponent<Image>().sprite = listAllFamilliarShotImage.familliarShotBlueSpread2;
                break;
        }
    }
}

[System.Serializable]
public class FamilliarShotAllImage
{
    public Sprite familliarShotGreenNormal;
    public Sprite familliarShotBlueNormal;
    public Sprite familliarShotOrangeNormal;
    public Sprite familliarShotVioletNormal;
    public Sprite familliarShotRedNormal;
    public Sprite familliarShotWhiteNormal;
    public Sprite familliarShotPinkNormal;
    public Sprite familliarShotYellowNormal;

    public Sprite familliarShotGreenSpread2;
    public Sprite familliarShotBlueSpread2;
    public Sprite familliarShotOrangeSpread2;
    public Sprite familliarShotVioletSpread2;
    public Sprite familliarShotRedSpread2;
    public Sprite familliarShotWhiteSpread2;
    public Sprite familliarShotPinkSpread2;
    public Sprite familliarShotYellowSpread2;

    public Sprite familliarShotGreenSpread3;
    public Sprite familliarShotBlueSpread3;
    public Sprite familliarShotOrangeSpread3;
    public Sprite familliarShotVioletSpread3;
    public Sprite familliarShotRedSpread3;
    public Sprite familliarShotWhiteSpread3;
    public Sprite familliarShotPinkSpread3;
    public Sprite familliarShotYellowSpread3;
}

[System.Serializable]
public class FamilliarShotContactImageAllObject
{
    public GameObject familliarShotContactGreenObject;
    public GameObject familliarShotContactBlueObject;
    public GameObject familliarShotContactOrangeObject;
    public GameObject familliarShotContactVioletObject;
    public GameObject familliarShotContactRedObject;
    public GameObject familliarShotContactWhiteObject;
    public GameObject familliarShotContactPinkObject;
    public GameObject familliarShotContactYellowObject;
}

[System.Serializable]
public class FamilliarShotStats
{
    public string shotDamage;
    public string CRTDamage;

    public float shotSize;
    public float moveSpeed;
    public float startDelay;
    public float CRTRate;
}