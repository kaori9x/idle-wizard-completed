using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class FamilliarDismantleController : MonoBehaviour
{
    /** ======= MARK: - Init ======= */
    public string GetClassName()
    {
        return this.GetType().Name;
    }

    private FamilliarDismantleController()
    {
        if (_instance == null)
            _instance = this;
    }

    /** ======= MARK: - Singleton ======= */

    private static FamilliarDismantleController _instance;

    public static FamilliarDismantleController instance
    {
        get
        {
            return _instance;
        }
    }
    // Start is called before the first frame update
    public int TotalSelected=0;
    public GameObject Prefab;
    private int LineCount;
    public List<FamilliarData> familliarInventory;
    public List<int> dismantleListIndex ;
    public Text dismantle;
    private List<EventListener> _eventListeners;
    private const string rRubyDismantle = "5";
    private const string sRRubyDismantle = "10";
    private const string sSRRubyDismantle = "20";
    private string totalRubyDismantle = "0";
    public GameObject familliarUpgrade;

    void Start()
    {
        AddListeners();
        ResetDismantleList();
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void AddFamilliar(FamilliarData data)
    {
        //Prefab = Resources.Load("FamilliarSlot") as GameObject;
        var copy = Instantiate(Prefab);
        UpdateSlot(copy.gameObject, data);
        copy.transform.parent = transform;
    }
    public void ResetDismantleList(){
        familliarInventory = SaveDataManager.instance.LoadFamilliarInventoryData();
        dismantleListIndex = new List<int>();
        for (int i=0; i <familliarInventory.Count; i++)
        {
            if( i < transform.childCount)
            {
                UpdateSlot(transform.GetChild(i).gameObject, familliarInventory[i]);
            }
            else { AddFamilliar(familliarInventory[i]); }

            List<int> equipedList = SaveDataManager.instance.LoadEquippedFamilliarData();
            if (equipedList.Exists(element => element == i))
            {
                Transform slot = transform.GetChild(i).transform.GetChild(0).transform.GetChild(1);
                if (!slot.GetComponent<FamilliarDismantleSlot>().Locked)
                {
                    slot.GetComponent<FamilliarDismantleSlot>().ToggleLock();
                }
            }
        }
        Debug.Log("Total Familliar in inventory: " + familliarInventory.Count);
        dismantleListIndex.Clear();
        FamilliarEquipController.instance.SetUpFamilliarInventory();
        FixSize();
    }
    public void DeleteDismantleList()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
    public void DeleteNumberOfChild(int number)
    {
        for(int i = transform.childCount- number; i < transform.childCount; i++) { 
            Destroy(transform.GetChild(i).gameObject);
        }
    }
    private void FixSize(){
        LineCount = (int)Math.Ceiling((double)transform.childCount / 6);
        if (LineCount > 2)
        {
            RectTransform dismantleInventory = transform.GetComponent<RectTransform>();
            dismantleInventory.sizeDelta = new Vector2(717, 110 * LineCount);
        }
        else
        {
            RectTransform dismantleInventory = transform.GetComponent<RectTransform>();
            dismantleInventory.sizeDelta = new Vector2(717, 300);
        }
    }
    public void DismantleSelected()
    {
        foreach (int index in dismantleListIndex.OrderByDescending(v => v))
        {
            familliarInventory.RemoveAt(index);
        }
        //Add ruby to save data
        CurrencyController.AddRuby(totalRubyDismantle);
        //Update inventory list
        SaveDataManager.instance.SaveFamilliarData_Overwrite(familliarInventory);
        dismantleListIndex.Clear();
        DeleteNumberOfChild(TotalSelected);
        Debug.Log("Child:    " + transform.childCount);
        ResetDismantleList();
        TotalSelected = 0;
    }
    public void SelectAllFamilliar()
    {
        for (int i = 0; i < familliarInventory.Count; i++)
        {
            if (!transform.GetChild(i).transform.GetChild(0).transform.GetChild(1).GetComponent<FamilliarDismantleSlot>().Locked)
            {
                transform.GetChild(i).transform.GetChild(0).transform.GetChild(1).GetComponent<FamilliarDismantleSlot>().Select();
            }
        }
    }
    private void UpdateSlot(GameObject slot , FamilliarData data)
    {
        Transform rarityImage = slot.transform.GetChild(0).transform.GetChild(0);
        Transform familliarImage = slot.transform.GetChild(0).transform.GetChild(1);

        rarityImage.GetComponent<Image>().sprite = Resources.Load<Sprite>(GetRarityImage(data.rarity));
        familliarImage.GetComponent<Image>().sprite = Resources.Load<Sprite>(data.image);
        slot.transform.GetChild(0).transform.GetChild(1).GetComponent<FamilliarDismantleSlot>().UnSelect();
        if (slot.transform.GetChild(0).transform.GetChild(1).GetComponent<FamilliarDismantleSlot>().Locked)
        {
            slot.transform.GetChild(0).transform.GetChild(1).GetComponent<FamilliarDismantleSlot>().ToggleLock();
        }
    }
    public void SetNotice()
    {
        totalRubyDismantle = "0";
        dismantle = GameObject.Find("DismantleSelected").transform.GetChild(1).GetComponent<Text>();
        dismantle.text = "Dismantle " + dismantleListIndex.Count + " familliars";
        foreach (int index in dismantleListIndex.OrderByDescending(v => v))
        {
            totalRubyDismantle = LargeNumberStaticUtil.Addition(totalRubyDismantle, GetRubyDismantle(familliarInventory[index].rarity));
        }
        GameObject.Find("RubyDismantleAmount").transform.GetChild(0).GetComponent<Text>().text = "Total: " + totalRubyDismantle;
    }
    public string GetRarityImage(string rarity)
    {
        string rarityImageString = "";
        switch (rarity)
        {
            case "R":
                rarityImageString = "Sprites/UI/Frames/Silver_square";
                break;
            case "SR":
                rarityImageString = "Sprites/UI/Frames/Gold_square";
                break;
            case "SSR":
                rarityImageString = "Sprites/UI/Frames/Rainbow Square";
                break;
        }
        return rarityImageString;
    }
    public void SetRankUI(GameObject RankUI, int currentRank)
    {
        for(int i=0;i< currentRank - 1; i++)
        {

        }
    }
    public string GetRubyDismantle(string rarity)
    {
        switch (rarity)
        {
            case "R":
                return rRubyDismantle;
            case "SR":
                return sRRubyDismantle;
            case "SSR":
                return sSRRubyDismantle;
        }
        return "";
    }
    private void AddListeners()
    {
        _eventListeners = new List<EventListener>
        {
           CustomEventSystem.instance.AddListener(EventCode.ON_EQUIP_FAMILLIAR, this, OnEquipFamilliar),
           CustomEventSystem.instance.AddListener(EventCode.ON_UNEQUIP_FAMILLIAR, this, OnUnEquipFamilliar),
        };
    }

    private void RemoveListeners()
    {
        if (_eventListeners.Count != 0)
        {
            foreach (EventListener listener in _eventListeners)
                CustomEventSystem.instance.RemoveListener(listener.eventCode, listener);
        }
    }
    public void OnEquipFamilliar(object[] eventParam)
    {

    }
    public void OnUnEquipFamilliar(object[] eventParam)
    {

    }
}
