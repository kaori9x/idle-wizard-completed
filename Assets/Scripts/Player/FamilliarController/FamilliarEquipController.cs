using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public  class FamilliarEquipController : MonoBehaviour
{
    public string GetClassName()
    {
        return this.GetType().Name;
    }

    private FamilliarEquipController()
    {
        if (_instance == null)
            _instance = this;
    }

    /** ======= MARK: - Singleton ======= */

    private static FamilliarEquipController _instance;

    public static FamilliarEquipController instance
    {
        get
        {
            return _instance;
        }
    }

    // Start is called before the first frame update
    public List<int> number = new List<int>();
    public int currentPage = 0;
    public int maxPage = 0;
    public List<FamilliarData> equipFamilliar = new List<FamilliarData>();
    public List<int> equipedList;
    private GameObject familliarInventory;
    private List<EventListener> _eventListeners;

    void Start()
    {
        AddListeners();
        equipedList = SaveDataManager.instance.LoadEquippedFamilliarData();
        equipFamilliar = SaveDataManager.instance.LoadFamilliarInventoryData();
        SetUpFamilliarInventory();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Nextpage()
    {
        if(currentPage < maxPage)
        {
            currentPage++;
            SetUpFamilliarInventory();
        }
    }
    public void PrevPage()
    {
        if (currentPage > 0)
        {
            currentPage--;
            SetUpFamilliarInventory();
        }

    }
    public void SetUpFamilliarInventory()
    {
        if (equipedList.Count == 0) return;
        for (int i = 0; i < 5; i++)
        {
            Debug.Log("==xx==xx== " + i);
            instance.familliarInventory.transform.GetChild(i).gameObject.SetActive(true);
            if (i + instance.currentPage * 5 > instance.equipFamilliar.Count - 1)
            {
                instance.familliarInventory.transform.GetChild(i).gameObject.SetActive(false);
                continue;
            }
            Transform familliar = instance.familliarInventory.transform.GetChild(i).transform.GetChild(0);
            familliar.GetChild(4).gameObject.SetActive(false);
            if (instance.currentPage == 0)
            {
                for (int j = 0; j < 4; j++)
                {
                    if ((5 * instance.currentPage + i) == instance.equipedList[j])
                    {
                        familliar.GetChild(4).gameObject.SetActive(true);
                    }
                }
            }
            familliar.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(FamilliarDismantleController.instance.GetRarityImage(instance.equipFamilliar[instance.currentPage * 5 + i].rarity));
            familliar.GetChild(1).GetComponent<Image>().sprite = Resources.Load<Sprite>(instance.equipFamilliar[instance.currentPage * 5 + i].image);
            familliar.GetChild(2).GetComponent<Text>().text = instance.equipFamilliar[instance.currentPage * 5 + i].name;
            familliar.GetChild(3).GetComponent<Text>().text = "Base damage: " + instance.equipFamilliar[instance.currentPage * 5 + i].stats.baseDamage;
        }
        for (int i = 0; i < 4; i++)
        {
            Transform equipSlot = instance.familliarInventory.transform.parent.GetChild(i);
            if (instance.equipedList[i] < 0)
            {
                SetBlank(equipSlot);
                continue;
            }
            equipSlot.GetChild(1).transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(FamilliarDismantleController.instance.GetRarityImage(instance.equipFamilliar[instance.equipedList[i]].rarity));
            equipSlot.GetChild(1).transform.GetChild(1).GetComponent<Image>().sprite = Resources.Load<Sprite>(instance.equipFamilliar[instance.equipedList[i]].image);
            equipSlot.GetChild(2).transform.GetChild(0).GetComponent<Text>().text = instance.equipFamilliar[instance.equipedList[i]].name;
            equipSlot.GetChild(2).transform.GetChild(1).GetComponent<Text>().text = "Level: 0";
            equipSlot.GetChild(2).transform.GetChild(2).GetComponent<Text>().text = "Rank: 0";
            equipSlot.GetChild(2).transform.GetChild(3).GetComponent<Text>().text = "Rarity: " + instance.equipFamilliar[instance.equipedList[i]].rarity;
        }
    }
    public  void InitFamilliarInventory()
    {
        familliarInventory = transform.gameObject;
        Debug.Log("==xx==xx== " + familliarInventory.name);
        equipFamilliar = SaveDataManager.instance.LoadFamilliarInventoryData();
        maxPage = (int)Math.Ceiling((double)equipFamilliar.Count / 5) - 1;
        currentPage = 0;
        equipedList = SaveDataManager.instance.LoadEquippedFamilliarData();
        if (equipedList.Count == 0)
        {
            for (int i = 0; i < 4; i++)
            {
                equipedList.Add((int)-1);
            }
        }
        SetUpFamilliarInventory();
    }

    public void SortFamilliarList()
    {
        List<FamilliarData> sortedList = instance.equipFamilliar;
        int count=0;
        for (int j = 0; j < 4; j++)
        {
            for (int i = 0; i < instance.equipFamilliar.Count; i++)
            {
                if(instance.equipedList[j] == i)
                {
                    FamilliarData temp = sortedList[count];
                    sortedList[count] = sortedList[i];
                    sortedList[i]  =temp;
                    instance.equipedList[j] = count;
                    break;
                }
            }
            if (instance.equipedList[j] != -1) count++;
        }
        SaveDataManager.instance.SaveFamilliarData_Overwrite(sortedList);
        SaveDataManager.instance.SaveEquippedFamilliar(instance.equipedList);
        SetUpFamilliarInventory();
    }
    private void AddListeners()
    {
        _eventListeners = new List<EventListener>
        {
           CustomEventSystem.instance.AddListener(EventCode.ON_EQUIP_FAMILLIAR, this, OnEquipFamilliar),
           CustomEventSystem.instance.AddListener(EventCode.ON_UNEQUIP_FAMILLIAR, this, OnUnEquipFamilliar),
        };
    }

    private void RemoveListeners()
    {
        if (_eventListeners.Count != 0)
        {
            foreach (EventListener listener in _eventListeners)
                CustomEventSystem.instance.RemoveListener(listener.eventCode, listener);
        }
    }
    public void OnEquipFamilliar(object[] eventParam)
    {
        
    }
    public void OnUnEquipFamilliar(object[] eventParam)
    {
       
    }
    private void SetBlank(Transform familliar)
    {
        familliar.GetChild(1).transform.GetChild(0).GetComponent<Image>().sprite = null;
        familliar.GetChild(1).transform.GetChild(1).GetComponent<Image>().sprite = null;
        familliar.GetChild(2).transform.GetChild(0).GetComponent<Text>().text = "Name";
        familliar.GetChild(2).transform.GetChild(1).GetComponent<Text>().text = "Level: ";
        familliar.GetChild(2).transform.GetChild(2).GetComponent<Text>().text = "Rank: ";
        familliar.GetChild(2).transform.GetChild(3).GetComponent<Text>().text = "Rarity: ";
    }
}
