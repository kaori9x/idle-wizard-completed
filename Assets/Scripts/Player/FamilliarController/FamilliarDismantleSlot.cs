using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FamilliarDismantleSlot : MonoBehaviour ,IPointerClickHandler
{
    public bool Selected = false;
    public bool Locked = false;
    public Image Familliar;
    public Image Lock;
    private int  childIndex;
    public Text familliarDetails;
    // Start is called before the first frame update
    void Start()
    {
        childIndex = transform.parent.gameObject.transform.parent.transform.GetSiblingIndex();
        familliarDetails = GameObject.Find("FamilliarDetailsInfo").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnPointerClick(PointerEventData eventData)
    {
        familliarDetails.text = "";
        familliarDetails.text += FamilliarDismantleController.instance.familliarInventory[childIndex].name;
        familliarDetails.text += "\n";
        familliarDetails.text += "Base Damage: "+FamilliarDismantleController.instance.familliarInventory[childIndex].stats.baseDamage;
        familliarDetails.text += "\n";
        familliarDetails.text += "Crit Rate: " + FamilliarDismantleController.instance.familliarInventory[childIndex].stats.CRTRate+"%";
        familliarDetails.text += "\n";
        for(int i=0;i< FamilliarDismantleController.instance.familliarInventory[childIndex].passiveSkills.Count; i++)
        {
            familliarDetails.text += "Rank";
            familliarDetails.text += FamilliarDismantleController.instance.familliarInventory[childIndex].passiveSkills[i].requiredRank+": ";
            familliarDetails.text += FamilliarDismantleController.instance.familliarInventory[childIndex].passiveSkills[i].name+"--";
            familliarDetails.text += FamilliarDismantleController.instance.familliarInventory[childIndex].passiveSkills[i].effectDesc;
            familliarDetails.text += "\n";
        }
    }
    public void ToggleLock()
    {
        Locked = !Locked;
        if (Locked)
        {
            if (Selected)
            {
                UnSelect();
            }
            Lock.sprite = Resources.Load<Sprite>("Sprites/UI/padlock");
        }
        else
        {
            Lock.sprite = Resources.Load<Sprite>("Sprites/UI/unlock");
        }
    }
    public void ToggleSelect()
    {
        if (!Selected)
        {
            Select();
        }
        else
        {
            UnSelect();
        }
    }
    public void Select(){
        if (!Selected)
        {
            Selected = true;
            FamilliarDismantleController.instance.TotalSelected++;
            Familliar.color = new Color32(100, 100, 255, 255);
            FamilliarDismantleController.instance.dismantleListIndex.Add(childIndex);
        }
    }
    public void UnSelect(){
        if (Selected)
        {
            Selected = false;
            Familliar.color = new Color32(255, 255, 255, 255);
            FamilliarDismantleController.instance.TotalSelected--;
            FamilliarDismantleController.instance.dismantleListIndex.Remove(childIndex);
        }

    }
    public void SelectDismantle()
    {
        if (Locked) return;
        ToggleSelect();
    }
}
