using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FamilliarRankUpController : MonoBehaviour
{
    public static int totalRankUp = 0;
    public GameObject Prefab;
    public List<int> similarFamilliarIndex= new List<int>();
    public static List<int> rankupContribute = new List<int>();
    private int LineCount;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void GetSimilarFamilliar(int index)
    {
        similarFamilliarIndex.Clear();
        DeleteDismantleList();
        totalRankUp = 0;
        for (int i=0;i< FamilliarDismantleController.instance.familliarInventory.Count; i++)
        {
            if (i == index) continue;
            if(FamilliarDismantleController.instance.familliarInventory[i].id == FamilliarDismantleController.instance.familliarInventory[index].id)
            {
                similarFamilliarIndex.Add(i);
            }
        }
        for(int i=0;i< similarFamilliarIndex.Count; i++)
        {
            var copy = Instantiate(Prefab);
            UpdateSlot(copy.gameObject, FamilliarDismantleController.instance.familliarInventory[index]);
            copy.transform.parent = transform;
        }
        FixSize();
    }
    private void UpdateSlot(GameObject slot, FamilliarData data)
    {
        Transform rarityImage = slot.transform.GetChild(0).transform.GetChild(0);
        Transform familliarImage = slot.transform.GetChild(0).transform.GetChild(1);
        rarityImage.GetComponent<Image>().sprite = Resources.Load<Sprite>(FamilliarDismantleController.instance.GetRarityImage(data.rarity));
        familliarImage.GetComponent<Image>().sprite = Resources.Load<Sprite>(data.image);
    }
    public void DeleteDismantleList()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
    private void FixSize()
    {
        LineCount = (int)Math.Ceiling((double)similarFamilliarIndex.Count / 5);
        Debug.Log("XXXXXXX" + similarFamilliarIndex.Count);
        Debug.Log("XXXXXXX" + LineCount);
        if (LineCount > 1)
        {
            RectTransform rankupInventory = transform.GetComponent<RectTransform>();
            rankupInventory.sizeDelta = new Vector2(717, 110 * LineCount);
        }
        else
        {
            RectTransform rankupInventory = transform.GetComponent<RectTransform>();
            rankupInventory.sizeDelta = new Vector2(717, 200);
        }
    }
    public static void AddToContributeList(int pos)
    {
        rankupContribute.Add(pos);
    }
    public static void RemoveFromContributeList(int pos)
    {
        rankupContribute.Remove(pos);
    }
    public void RankUp()
    {

    }
}
