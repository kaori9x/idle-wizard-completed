using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FamilliarRankUpSlot : MonoBehaviour , IPointerClickHandler
{
    public bool Selected = false;
    public Image Familliar;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        ToggleSelect();
    }
    public void ToggleSelect()
    {
        if (!Selected)
        {
            Select();
        }
        else
        {
            UnSelect();
        }
    }
    public void Select()
    {
        if (FamilliarRankUpController.totalRankUp == 5) return;
        if (!Selected)
        {
            Selected = true;
            FamilliarRankUpController.totalRankUp++;
            FamilliarRankUpController.AddToContributeList(transform.GetSiblingIndex());
            Familliar.color = new Color32(100, 100, 255, 255);
        }
    }
    public void UnSelect()
    {
        if (Selected)
        {
            Selected = false;
            Familliar.color = new Color32(255, 255, 255, 255);
            FamilliarRankUpController.totalRankUp--;
            FamilliarRankUpController.RemoveFromContributeList(transform.GetSiblingIndex());
        }
    }
}
