using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrencyController : MonoBehaviour
{
    // Start is called before the first frame update
    public static CurrencyData currency;
    private static string storedGold="0";
    private static string storedRuby="0";
    private static string storedDiamond="0";
    [SerializeField]
    public static Text diamondLayOut;
    public static Text goldLayOut;
    public static Text rubyLayOut;

    void Start()
    {
        string data = SaveDataManager.instance.DoLoadData(SaveDataManager.currencyFileName);
        if (data == "")
        {
            currency = new CurrencyData();
        }
        else
        {
            currency = JsonUtility.FromJson<CurrencyData>(data);
        }
        diamondLayOut = GameObject.Find("DiamondLayout").transform.GetChild(2).GetComponent<Text>();
        goldLayOut = GameObject.Find("GoldLayout").transform.GetChild(2).GetComponent<Text>();
        rubyLayOut = GameObject.Find("RubyLayout").transform.GetChild(2).GetComponent<Text>();
        diamondLayOut.text = currency.diamond;
        goldLayOut.text = currency.gold;
        rubyLayOut.text = currency.ruby;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public static void AddDiamond(string addedAmount)
    {
        string temp = currency.diamond;
        if (LargeNumberStaticUtil.Addition(currency.diamond, storedDiamond) != "Can't caculate")
        {
            currency.diamond = LargeNumberStaticUtil.Addition(currency.diamond, storedDiamond);
        }
        if (temp != currency.diamond)
        {
            storedDiamond = "";
            temp = currency.diamond;
        }
        if (LargeNumberStaticUtil.Addition(currency.diamond, addedAmount) != "Can't caculate")
        {
            currency.diamond = LargeNumberStaticUtil.Addition(currency.diamond, addedAmount);
            if (currency.diamond == temp)
            {
                storedDiamond = LargeNumberStaticUtil.Addition(storedDiamond, addedAmount);
            }
        }
        SaveDataManager.instance.SaveCurrencyData(currency);
        diamondLayOut.text = currency.diamond;
    }
    public static void AddGold(string addedAmount)
    {
        string temp = currency.gold;
        if(LargeNumberStaticUtil.Addition(currency.gold, storedGold)!= "Can't caculate")
        {
            currency.gold = LargeNumberStaticUtil.Addition(currency.gold, storedGold);
        }
        if (temp != currency.gold)
        {
            storedGold = "";
            temp = currency.gold;
        }
        if (LargeNumberStaticUtil.Addition(currency.gold, addedAmount) != "Can't caculate")
        {
            currency.gold = LargeNumberStaticUtil.Addition(currency.gold, addedAmount);
            if (currency.gold == temp)
            {
                storedGold = LargeNumberStaticUtil.Addition(storedGold, addedAmount);
            }
        }
        SaveDataManager.instance.SaveCurrencyData(currency);
        goldLayOut.text = currency.gold;
    }
    public static void AddRuby(string addedAmount)
    {
        string temp = currency.ruby;
        if (LargeNumberStaticUtil.Addition(currency.ruby, storedRuby) != "Can't caculate")
        {
            currency.ruby = LargeNumberStaticUtil.Addition(currency.ruby, storedRuby);
        }
        if (temp != currency.ruby)
        {
            storedRuby = "";
            temp = currency.ruby;
        }
        if (LargeNumberStaticUtil.Addition(currency.ruby, addedAmount) != "Can't caculate")
        {
            currency.ruby = LargeNumberStaticUtil.Addition(currency.ruby, addedAmount);
            if (currency.ruby == temp)
            {
                storedRuby = LargeNumberStaticUtil.Addition(storedRuby, addedAmount);
            }
        }
        SaveDataManager.instance.SaveCurrencyData(currency);
        rubyLayOut.text = currency.ruby;
    }
    public static void RemoveDiamond(string removedAmount)
    {
        if(LargeNumberStaticUtil.Subtraction(currency.diamond, removedAmount)!= "Can't caculate")
        {
            currency.diamond = LargeNumberStaticUtil.Subtraction(currency.diamond, removedAmount);
        }
        SaveDataManager.instance.SaveCurrencyData(currency);
        diamondLayOut.text = currency.diamond;
    }
    public static void RemoveGold(string removedAmount)
    {
        if (LargeNumberStaticUtil.Subtraction(currency.gold, removedAmount) != "Can't caculate")
        {
            currency.gold = LargeNumberStaticUtil.Subtraction(currency.gold, removedAmount);
        }
        SaveDataManager.instance.SaveCurrencyData(currency);
        goldLayOut.text = currency.gold;
    }
    public static void RemoveRuby(string removedAmount)
    {
        if (LargeNumberStaticUtil.Subtraction(currency.ruby, removedAmount) != "Can't caculate")
        {
            currency.ruby = LargeNumberStaticUtil.Subtraction(currency.ruby, removedAmount);
        }
        SaveDataManager.instance.SaveCurrencyData(currency);
        rubyLayOut.text = currency.ruby;
    }
}
