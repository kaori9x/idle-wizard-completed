using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
public class PauseMenuEvent : MonoBehaviour
{
    public AudioSource AudioSource;
    public GameOffBonus Bonus;
    [SerializeField] Slider VolumeSlider;
    private float MusicVolume;
    private string key;
    private void Start() {
        key = "Volume" + AudioSource.name;
        float Volume = 0.5f;
        if(PlayerPrefs.HasKey(key))
        {
            Volume = PlayerPrefs.GetFloat(key);
        }
        AudioSource.volume = Volume;
        VolumeSlider.value = Volume;
        Debug.Log(AudioSource.name);
    }
    public void ReturnMainMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        GameOffBonus.SetLogOutDT();
    }
    public void QuitGame()
    {
        Debug.Log("Quit Game");
        Application.Quit();
    }
    public void ChangeVolume()
    {
        PlayerPrefs.SetFloat(key, VolumeSlider.value);
        MusicVolume = VolumeSlider.value;
        AudioSource.volume = VolumeSlider.value;
    }
}
