using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class TabController : MonoBehaviour
{
    public Transform TabPanels;
    public Transform TabButtons;
    private int selectedIndex;
    private TabButton selectedButton;
    private List<TabButton> buttons = new List<TabButton>();
    private List<Transform> panels = new List<Transform>();
    // Start is called before the first frame update

    private void Start() {
        for (int i = 0; i < TabButtons.transform.childCount; i++)
        {
            GameObject ButtonGo = TabButtons.transform.GetChild(i).gameObject;
            TabButton button = ButtonGo.GetComponent<TabButton>();
            button.setIndex(i);
            buttons.Add(button);
        }
        foreach (Transform item in TabPanels.transform)
        {
            panels.Add(item);
        }
        ButtonMouseClick(0);
    }
    public void ButtonMouseClick (int index){
        if(selectedButton!=null){
            selectedButton.ToggleActive();
        }
        selectedIndex = index;
        selectedButton = buttons[selectedIndex];
        selectedButton.ToggleActive();
        HideAllPanels();
    }
    public void ButtonMouseEnter (int index){
        
    }
    public void ButtonMouseExit (int index){
        
    }
    public void HideAllPanels(){
        for (int i = 0; i < panels.Count; i++)
        {
            if(i== selectedIndex){
                panels[i].gameObject.SetActive(true);
            }
            else panels[i].gameObject.SetActive(false);
        }
    }
}
