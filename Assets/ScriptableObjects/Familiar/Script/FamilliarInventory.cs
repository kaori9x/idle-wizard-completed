using UnityEngine;
using System.Collections.Generic;
[CreateAssetMenu(fileName = "FamilliarInventory", menuName = "idle-wizard/FamilliarInventory", order = 0)]
public class FamilliarInventory : ScriptableObject {
    public List<Familliar> FInventoty = new List<Familliar>();

    public void AddFamilliar(Familliar NewFamilliar){
        FInventoty.Add(NewFamilliar);
        ResetFamilliarID();
    }
    public void ResetFamilliarID(){
        for (int i = 0; i < FInventoty.Count; i++)
        {
            FInventoty[0].SetFamilliarId(i);
        }
    }
}